<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php
global $wpdb;
$apps_table = $wpdb->prefix . "jf_endorsements";
$qstring = "";

if(isset($_POST['importSubmit'])){
    
    //validate whether uploaded file is a csv file
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            //skip first line
            fgetcsv($csvFile);
            
            //parse data from csv file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){
                //check whether member already exists in database with same name
					$wpdb->get_results("SELECT id FROM ".$apps_table." WHERE id = '".$line[0]."'");
                if($wpdb->num_rows > 0){
                    //update member data
                   $wpdb->get_results("UPDATE ".$apps_table." SET first_name = '".$line[1]."', last_name = '".$line[2]."', affiliation = '".$line[3]."', category = '".$line[4]."' WHERE  id = '".$line[0]."'");
                }else{
                    //insert member data into database
                   $wpdb->get_results("INSERT INTO ".$apps_table." (id, first_name, last_name, affiliation, category) VALUES ('".$line[0]."','".$line[1]."','".$line[2]."','".$line[3]."','".$line[4]."')");
                }
            }
            
            //close opened csv file
            fclose($csvFile);

            $qstring = '<p>File uploaded successfully.</p>';

    		if($wpdb->last_error !== '') {

				//$str   = htmlspecialchars( $wpdb->last_result, ENT_QUOTES );
				$query = htmlspecialchars( $wpdb->last_query, ENT_QUOTES );

				print "<div id='error'>
				<p class='wpdberror'><strong>WordPress database error!</strong><br />
				<code>$query</code></p>
				</div>";
			}
        }else{
            $qstring = '<p>There was an error uploading your file.</p>';
        }
    }else{
        $qstring = '<p>Invalid file.</p>';
    }
}

?>
<?php if ($qstring != '') { echo $qstring; } //generic success notice ?> 

<form action="" method="post" enctype="multipart/form-data" name="upload_endorsements" id="upload_endorsements"> 
  Choose your file: <br /> 
  <input name="file" type="file" id="csv" /> <br><br>
  <input type="submit" name="importSubmit" value="Import .csv File" /> 
</form> 
