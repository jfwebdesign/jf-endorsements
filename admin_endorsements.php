<?php
/**
 * Plugin Name: Endorsements List and Updating
 * Plugin URI: http://jfwebdesign.com
 * Description: Creates an endorsment list and a way to update them.
 * Version: 1.1
 * Author: JF WebDesign
 * Author URI: http://jfwebdesign.com
 */
$plugin_url = 'jf-endorsements/';

function rel_endorsements_app(){
	global $wpdb;
	$sql = "";
	//create the name of the table including the wordpress prefix 
	$apps_table = $wpdb->prefix . "jf_endorsements";
	//$wpdb->show_errors(); 
	
	//check if there are any tables of that name already
	if($wpdb->get_var("show tables like '$apps_table'") !== $apps_table) 
	{  
		$sql = "CREATE TABLE $apps_table (
		  id int(11) NOT NULL AUTO_INCREMENT,
		  first_name VARCHAR(500) NOT NULL,
		  last_name VARCHAR(500) NOT NULL,
		  affiliation VARCHAR(500) NOT NULL,
		  category int(11) NOT NULL,
		  list_order int(11) NOT NULL,
		  UNIQUE KEY id (id)
		);";
  
  }	
	//include the wordpress db functions
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql);
	
	//register the new table with the wpdb object
	if (!isset($wpdb->donors)) 
	{
		$wpdb->donors = $apps_table; 
		//add the shortcut so you can use $wpdb->profile_apps
		$wpdb->tables[] = str_replace($wpdb->prefix, '', $apps_table); 
	}
} 
//add to front and backend inits
add_action('init', 'rel_endorsements_app');
// Hook for adding admin menus
add_action('admin_menu', 'add_endorsements_menu');
function add_endorsements_menu() {
    // Manage Endorsements Page:
    add_menu_page(__('Manage Endorsements','endorsement_1'), __('Manage Endorsements','endorsement_1'), 'manage_options', 'endor-top-level-handle', 'endor_toplevel_page' );
	//Endorsement List
	add_submenu_page('endor-top-level-handle', __('List Endorsements','endorsement_1'), __('List Endorsements','endorsement_1'), 'manage_options', 'list_endorsements', 'endor_list_endorsements');
	// Add Endorsements:
    add_submenu_page('endor-top-level-handle', __('Add Endorsement','endorsement_1'), __('Add Endorsement','endorsement_1'), 'manage_options', 'add_endorsements', 'endor_add_endorsements');
	// Upload Endorsements:
	add_submenu_page('endor-top-level-handle', __('Upload Endorsements','endorsement_1'), __('Upload Endorsements','endorsement_1'), 'manage_options', 'upload_endorsements', 'endor_upload_endorsements');
	//Edit Endorsements
	add_submenu_page(NULL, __('Edit Endorsement','endorsement_1'), __('Edit Endorsement','endorsement_1'), 'manage_options', 'edit_endorsements', 'endor_edit_endorsements');

}

// Manage Donors back end page
function endor_toplevel_page() {
    echo "<h2>" . __( 'Read Me', 'endorsement_1' ) . "</h2>";
	//Plugin Pieces
	require_once( plugin_dir_path( __FILE__ ) . 'endorsements_info.php' );
}
//List Endorsements
function endor_list_endorsements() {
    echo "<h2>" . __( 'List Endorsements', 'endorsement_1' ) . "</h2>";
	//Plugin Pieces
	require_once( plugin_dir_path( __FILE__ ) . 'admin_list_endorsements.php' );
}
// Add Endorsements
function endor_add_endorsements() {
    echo "<h2>" . __( 'Add Endorsements', 'endorsement_1' ) . "</h2>";
	//Plugin Pieces
	require_once( plugin_dir_path( __FILE__ ) . 'add_endorsement.php' );
}
// Edit Endorsements
function endor_edit_endorsements() {
    echo "<h2>" . __( 'Edit Endorsement', 'endorsement_2' ) . "</h2>";
	//Plugin Pieces
	require_once( plugin_dir_path( __FILE__ ) . 'edit_endorsement.php' );
}
// Upload Endorsements
function endor_upload_endorsements() {
    echo "<h2>" . __( 'Upload Endorsements', 'endorsement_1' ) . "</h2><p>Must be in .csv format.</p>";
	//Plugin Pieces
	require_once( plugin_dir_path( __FILE__ ) . 'upload_endorsements.php' );
}
require_once( plugin_dir_path( __FILE__ ) . 'endorsements.php' );
?>