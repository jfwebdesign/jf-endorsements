<?php
	global $wpdb;
	$apps_table = $wpdb->prefix . "jf_endorsements";
	$get_categories = $wpdb->get_results("SELECT * FROM ".$apps_table . " group by category;");
	$get_columns = $wpdb->get_results("SHOW COLUMNS FROM ".$apps_table . ";");


	$path = 'admin.php?page=edit_endorsements';
	$url = admin_url($path);
//ob_start();
	echo '<h3>Shortcode Instructions</h3>';
	echo '<p>To insert an endorsement list into a page, use the following shortcode: [jf_endorsements category= orderby=].</p>';
	echo '<p><b>Options for categories are:</b><br>';

	foreach($get_categories as $category_number) 
		{
			echo $category_number->category .'<br>';
		}
	echo '</p>';
	echo '<p><b>Options for orderby are:</b><br>';

	foreach($get_columns as $column_name) 
			{
				echo $column_name->Field .'<br>';
			}
		echo '</p>';

echo '<h3>Upload Instructions</h3>';
echo '<p>Lists may be uploaded as long as they are saved as a .csv file, with the columns:<br>';
foreach($get_columns as $column_name) 
			{
				echo $column_name->Field .'<br>';
			}
echo '</p>';
//return ob_get_clean();
?>