<?php
global $wpdb;
	$apps_table = $wpdb->prefix . "jf_endorsements";
	$get_names = $wpdb->get_results("SELECT * FROM ".$apps_table . " order by last_name ASC;");
	$columns = 2;
	$path = 'admin.php?page=edit_endorsements';
	$url = admin_url($path);
//ob_start();
	echo '<style>
	.list_columns
		{
			display: flex;
			flex-wrap:wrap;
			flex-direction:row;
			justify-content:flex-start;
			align-items:stretch;
			list-style: none;
			width: 100%;
		}
		.column_item {
			width: 50%;
			}	
	
	@media screen and (max-width: 44.375em) {
		.list_columns
		{
			display: block;
			width: 100%;
		}
		.column_item 
		{
			width: 100%;
		}
	</style>';
echo "<h3>Records are in alphabetical order by last name.</h3>";
	echo '<div class="list_wrap"><div class="list_columns">';
		foreach($get_names as $name) 
		{
			echo "<div class='column_item'>";
			echo "<b>".$name->first_name. " ".$name->last_name. "</b><br />";
			if ($name->affiliation != ''){echo $name->affiliation. "<br />";}
			echo "<a href='".$url."&eid=".$name->id."'>Edit</a>";
			echo "<br /><br /></div>";
		}
	echo "</div></div>";
//return ob_get_clean();
?>