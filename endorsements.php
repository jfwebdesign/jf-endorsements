<?php



//add button in shortcode

add_shortcode('jf_endorsements', 'load_jf_endorsements'); // [jf_endorsements category=]

function load_jf_endorsements($atts)
{
	$category = '';
			$orderby = '';
	// Attributes
	extract( shortcode_atts(
		array(
			$category => '',
			$orderby => '',
		), $atts )
	);
	$attributes = array($atts); 
	foreach ($attributes as $value) {
	$category = $value['category'];
	$orderby = $value['orderby'];
	}
	
	global $wpdb;
	$apps_table = $wpdb->prefix . "jf_endorsements";
	$get_names = $wpdb->get_results("SELECT * FROM ".$apps_table . " order by ".$orderby.";");
ob_start();
	echo '<style>
	.list_columns
		{
			display: flex;
			flex-wrap:wrap;
			flex-direction:row;
			justify-content:flex-start;
			align-items:stretch;
			list-style: none;
			width: 100%;
		}
		.column_item 
		{
			width: 50%;
			text-align: center;
		}	
	
	@media screen and (max-width: 44.375em) {
		.list_columns
		{
			display: block;
			width: 100%;
		}
		.column_item 
		{
			width: 100%;
		}
	</style>';
	echo '<div class="list_columns">';
		foreach($get_names as $name) 
		{
			if ($name->category == $category) 
			{
				echo "<div class='column_item cat_".$category."'><span style='text-align: left;'>";
				echo "<b>".stripslashes($name->first_name). " ".stripslashes($name->last_name). "</b><br />";
				if ($name->affiliation != ''){echo stripslashes($name->affiliation). "<br />";}
				echo "<br /></span></div>";
			}
		}
	echo "</div>";
return ob_get_clean();
}


